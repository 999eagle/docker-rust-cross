FROM rust:latest
LABEL maintainer="Sophie Tauchert <sophie@999eagle.moe>"

# Rust targets

RUN rustup target add x86_64-pc-windows-gnu && \
	rustup target add x86_64-apple-darwin

# apt packages

RUN apt-get update && \
	apt-get -y install mingw-w64 clang llvm-dev libxml2-dev uuid-dev libssl-dev bash patch make tar xz-utils bzip2 gzip sed cpio && \
	apt-get clean

# Windows compiler

RUN ln -s windows.h /usr/x86_64-w64-mingw32/include/Windows.h && \
	ln -s shlobj.h /usr/x86_64-w64-mingw32/include/Shlobj.h

# MacOS compiler

ENV OSXCROSS_REPO="tpoechtrager/osxcross" \
	OSXCROSS_REVISION="ee54d9fd43b45947ee74c99282b360cd27a8f1cb" \
	DARWIN_SDK_VERSION="10.11" \
	DARWIN_OSX_VERSION_MIN="10.7" \
	DARWIN_VERSION="15" \
	DARWIN_SDK_URL="https://s3.dockerproject.org/darwin/v2/MacOSX${DARWIN_SDK_VERSION}.sdk.tar.xz"

RUN mkdir -p "/tmp/osxcross" && \
	cd "/tmp/osxcross" && \
	curl -sSLo osxcross.tar.gz "https://codeload.github.com/${OSXCROSS_REPO}/tar.gz/${OSXCROSS_REVISION}" && \
	tar --strip=1 -xzf osxcross.tar.gz && \
	curl -sSLo tarballs/MacOSX${DARWIN_SDK_VERSION}.sdk.tar.xz "${DARWIN_SDK_URL}" && \
	UNATTENDED=1 SDK_VERSION="${DARWIN_SDK_VERSION}" OSX_VERSION_MIN="${DARWIN_OSX_VERSION_MIN}" ./build.sh && \
	mv target /usr/osxcross && \
	rm -rf /tmp/osxcross && \
	rm -rf "/usr/osxcross/SDK/MacOSX${DARWIN_SDK_VERSION}.sdk/usr/share/man"

ENV PATH="/usr/osxcross/bin:${PATH}"
WORKDIR /build
